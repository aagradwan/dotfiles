import System.IO
import System.Exit

import XMonad
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers(doFullFloat, doCenterFloat, isFullscreen, isDialog)
import XMonad.Config.Desktop
import XMonad.Config.Azerty
import XMonad.Util.Run(spawnPipe)
import XMonad.Actions.SpawnOn
import XMonad.Util.EZConfig (additionalKeys, additionalMouseBindings)
import XMonad.Actions.CycleWS
import XMonad.Hooks.UrgencyHook
import qualified Codec.Binary.UTF8.String as UTF8

import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.ResizableTile
---import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen (fullscreenFull)
import XMonad.Layout.Cross(simpleCross)
import XMonad.Layout.Spiral(spiral)
import XMonad.Layout.ThreeColumns
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.IndependentScreens
import XMonad.Layout.Tabbed


import XMonad.Layout.CenteredMaster(centerMaster)

import Graphics.X11.ExtraTypes.XF86
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import qualified Data.ByteString as B
import Control.Monad (liftM2)
import qualified DBus as D
import qualified DBus.Client as D

import XMonad.Prompt
import XMonad.Prompt.Input
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Man
import XMonad.Prompt.Shell
import XMonad.Prompt.Unicode
import XMonad.Prompt.XMonad
import Control.Arrow (first)

import XMonad.Util.EZConfig (additionalKeysP)
myStartupHook = do
    spawn "$HOME/.xmonad/scripts/autostart.sh"
    setWMName "LG3D"
    spawn "setxkbmap -layout us,ar  -option 'grp:alt_shift_toggle'"
    spawn "wal -i /home/radwan/Downloads/Firewatch_5K_Wallpapers_HD_Wallpapers.jpg "
-- colours
normBord = "#4c566a"
focdBord = "#5e81ac"
fore     = "#DEE3E0"
back     = "#282c34"
winType  = "#c678dd"

--mod4Mask= super key
--mod1Mask= alt key
--controlMask= ctrl key
--shiftMask= shift key

myModMask = mod4Mask
encodeCChar = map fromIntegral . B.unpack
myFocusFollowsMouse = True
myBorderWidth = 3
myWorkspaces    = ["\61612","\61899","\61947","\61635","\61502","\61501","\61705","\61564","\62150","\61872"]
--myWorkspaces    = ["1","2","3","4","5","6","7","8","9","10"]
--myWorkspaces    = ["I","II","III","IV","V","VI","VII","VIII","IX","X"]

myBaseConfig = desktopConfig

-- window manipulations
myManageHook = composeAll . concat $
    [ [isDialog --> doCenterFloat]
    , [className =? c --> doCenterFloat | c <- myCFloats]
    , [title =? t --> doFloat | t <- myTFloats]
    , [resource =? r --> doFloat | r <- myRFloats]
    , [resource =? i --> doIgnore | i <- myIgnores]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61612" | x <- my1Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61899" | x <- my2Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61947" | x <- my3Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61635" | x <- my4Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61502" | x <- my5Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61501" | x <- my6Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61705" | x <- my7Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61564" | x <- my8Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\62150" | x <- my9Shifts]
    -- , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "\61872" | x <- my10Shifts]
    ]
    where
    -- doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
    myCFloats = ["Arandr", "Arcolinux-tweak-tool.py", "Arcolinux-welcome-app.py", "Galculator", "feh", "mpv", "Xfce4-terminal"]
    myTFloats = ["Downloads", "Save As..."]
    myRFloats = []
    myIgnores = ["desktop_window"]
    -- my1Shifts = ["Chromium", "Vivaldi-stable", "Firefox"]
    -- my2Shifts = []
    -- my3Shifts = ["Inkscape"]
    -- my4Shifts = []
    -- my5Shifts = ["Gimp", "feh"]
    -- my6Shifts = ["vlc", "mpv"]
    -- my7Shifts = ["Virtualbox"]
    -- my8Shifts = ["Thunar"]
    -- my9Shifts = []
    -- my10Shifts = ["discord"]




myLayout = spacingRaw True (Border 0 5 5 5) True (Border 5 5 5 5) True $ avoidStruts $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) $ tiled ||| Mirror tiled ||| spiral (6/7)  ||| ThreeColMid 1 (3/100) (1/2) ||| Full
    where
        tiled = Tall nmaster delta tiled_ratio
        nmaster = 1
        delta = 3/100
        tiled_ratio = 1/2


myMouseBindings (XConfig {XMonad.modMask = modMask}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modMask, 1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modMask, 2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modMask, 3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

    ]


-- keys config

myKeys :: [(String, X ())]
myKeys =

  -- SUPER + FUNCTION KEYS

  [
  --  ("M-e" ,spawn $ "emacs" )
   ("M-b" ,spawn $ "conky-toggle" )
  , ("M-f" ,sendMessage $ Toggle NBFULL)
  , ("M-h" ,spawn $ "kitty   bpytop" )
  , ("M-m" ,spawn $ "pragha" )
  , ("M-c" ,kill )
  , ("M-r" ,spawn $ "rofi-theme-selector" )
  , ("M-v" ,spawn $ "pavucontrol" )
  , ("M-l" ,spawn $ "arcolinux-logout" )
  --, (M xK_Escape spawn $ "xkill" )
  , ("M-<Return>" ,spawn $ "kitty" )
  , ("M-i" ,spawn $ "inkscape" )
  , ("M-g" ,spawn $ "gimp" )
  , ("M-S-v", spawn $ "virtualbox" )
  , ("M-z" ,spawn $ "rofi -show drun" )

  -- FUNCTION KEYS
  --, ((0, xK_F12), spawn $ "xfce4-terminal --drop-down" )

  -- SUPER + SHIFT KEYS

  , ("M-S-<Return>",  spawn $ "kitty ranger")
  , ("M-S-d" ,spawn $ "dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'")
  , ("M-S-r", spawn $ "xmonad --recompile && xmonad --restart")
  , ("M-S-q", io exitSuccess)             -- Quits xmonad
--  , ((modMask  , xK_c), kill)
  ,("M-S-k", spawn $ "kdeconnect-app" )
  -- , ((modMask .|. shiftMask , xK_x ), io (exitWith ExitSuccess))

  -- CONTROL + ALT KEYS

--  , ((controlMask .|. mod1Mask , xK_Next ), spawn $ "conky-rotate -n")
--  , ((controlMask .|. mod1Mask , xK_Prior ), spawn $ "conky-rotate -p")
  , ("M-a", spawn $ "xfce4-appfinder")
  , ("C-M1-b" , spawn $ "thunar")
  , ("C-M1-c" , spawn $ "catfish")
  , ("C-M1-e" , spawn $ "arcolinux-tweak-tool")
  , ("S-M-l", spawn $ "./Applications/LibreWolf-85.0-1.x86_64.AppImage")
  --, ((controlMask .|. mod1Mask , xK_g ), spawn $ "chromium -no-default-browser-check")
  --, ((controlMask .|. mod1Mask , xK_i ), spawn $ "nitrogen")
  --, ((controlMask .|. mod1Mask , xK_k ), spawn $ "arcolinux-logout")
--  , ((controlMask .|. mod1Mask , xK_l ), spawn $ "arcolinux-logout")
  , ("C-M1-m" , spawn $ "xfce4-settings-manager")
  , ("C-M1-o" , spawn $ "$HOME/.xmonad/scripts/picom-toggle.sh")
  , ("C-M1-p" , spawn $ "pamac-manager")
  , ("C-M1-r" , spawn $ "rofi-theme-selector")
  , ("C-M1-u" , spawn $ "pavucontrol")

  -- ALT + ... KEYS

  , ("M-q", spawn $ "xmonad --restart" )
  , ("M1-F2", spawn $ "gmrun" )
  , ("M1-F3", spawn $ "xfce4-appfinder" )

  --VARIETY KEYS WITH PYWAL

--  , ((mod1Mask .|. shiftMask , xK_u ), spawn $ "wal -i $(cat $HOME/.config/variety/wallpaper/wallpaper.jpg.txt)&")

  --CONTROL + SHIFT KEYS


  --SCREENSHOTS

  , ("<Print>" , spawn $ "xfce4-screenshooter" )
  , ("M-<Print>" , spawn $ "gnome-screenshot -i ")


  --MULTIMEDIA KEYS

  -- Mute volume
  , ("<xF86XK_AudioMute>", spawn $ "amixer -q set Master toggle")

  -- Decrease volume
  , ("<xF86XK_AudioLowerVolume>", spawn $ "amixer -q set Master 5%-")

  -- Increase volume
  , ("<xF86XK_AudioRaiseVolume>", spawn $ "amixer -q set Master 5%+")

  -- Increase brightness
  , ("<xF86XK_MonBrightnessUp>",  spawn $ "xbacklight -inc 25")

  -- Decrease brightness
  , ( "<xF86XK_MonBrightnessDown>", spawn $ "xbacklight -dec 25")

--  , ((0, xF86XK_AudioPlay), spawn $ "mpc toggle")
--  , ((0, xF86XK_AudioNext), spawn $ "mpc next")
--  , ((0, xF86XK_AudioPrev), spawn $ "mpc prev")
--  , ((0, xF86XK_AudioStop), spawn $ "mpc stop")

--  , ((0, xF86XK_AudioPlay), spawn $ "playerctl play-pause")
  --, ((0, xF86XK_AudioNext), spawn $ "playerctl next")
    --, ((0, xF86XK_AudioPrev), spawn $ "playerctl previous")
      --, ((0, xF86XK_AudioStop), spawn $ "playerctl stop")

  --------------------------------------------------------------------
  --  XMONAD LAYOUT KEYS

  -- Cycle through the available layout algorithms.
  , ("M-<space>", sendMessage NextLayout)

  --Focus selected desktop
  --, ((mod1Mask, xK_Tab), nextWS)

  --Focus selected desktop
  , ("M-<Tab>", nextWS)

  --Focus selected desktop
  , ("C-M1-<Left>" , prevWS)

  --Focus selected desktop
  , ("C-M1-<Right>" , nextWS)

  --  Reset the layouts on the current workspace to default.
  , ("M-S-<space>", setLayout $ XMonad.layoutHook conf)

  -- Move focus to the next window.
  , ("M-j" , windows W.focusDown)

  -- Move focus to the previous window.
  , ("M-k", windows W.focusUp  )

  -- Move focus to the master window.
  , ("M-S-m", windows W.focusMaster  )

  -- Swap the focused window with the next window.
  , ("M-S-j", windows W.swapDown  )

  -- Swap the focused window with the next window.
  , ("C-M-<Down>", windows W.swapDown  )

  -- Swap the focused window with the previous window.
  , ("M-S-k", windows W.swapUp    )

  -- Swap the focused window with the previous window.
  , ("C-M-<Up>", windows W.swapUp  )

  -- Shrink the master area.
  , ("C-S-h", sendMessage Shrink)

  -- Expand the master area.
  , ("C-S-l", sendMessage Expand)

  -- Push window back into tiling.
  , ("C-S-t" , withFocused $ windows . W.sink)

  -- Increment the number of windows in the master area.
  , ("C-M-<Left>", sendMessage (IncMasterN 1))

  -- Decrement the number of windows in the master area.
  , ("C-M-<Right>" , sendMessage (IncMasterN (-1)))
      -- Emacs (CTRL-e followed by a key)
  , ("M-e e", spawn "emacsclient -c -a 'emacs'")                            -- start emacs
  , ("M-e b", spawn "emacsclient -c -a 'emacs' --eval '(ibuffer)'")         -- list emacs buffers
  , ("M-e d", spawn "emacsclient -c -a 'emacs' --eval '(dired nil)'")       -- dired emacs file manager
  , ("M-e i", spawn "emacsclient -c -a 'emacs' --eval '(erc)'")             -- erc emacs irc client
  , ("M-e m", spawn "emacsclient -c -a 'emacs' --eval '(mu4e)'")            -- mu4e emacs email client
  , ("M-e n", spawn "emacsclient -c -a 'emacs' --eval '(elfeed)'")          -- elfeed emacs rss client
  , ("M-e s", spawn "emacsclient -c -a 'emacs' --eval '(eshell)'")          -- eshell within emacs
  , ("M-e t", spawn "emacsclient -c -a 'emacs' --eval '(mastodon)'")        -- mastodon within emacs
  , ("M-e v", spawn "emacsclient -c -a 'emacs' --eval '(+vterm/here nil)'") -- vterm within emacs
  -- emms is an emacs audio player. I set it to auto start playing in a specific directory.
  --, ("C-e a", spawn "emacsclient -c -a 'emacs' --eval '(emms)' --eval '(emms-play-directory-tree \"~/Music/Non-Classical/70s-80s/\")'")

  ]
  ++

  -- mod-[1..9], Switch to workspace N
  -- mod-shift-[1..9], Move client to workspace N
  [((m .|. modMask, k), windows $ f i)

  --Keyboard layouts
  --qwerty users use this line
   | (i, k) <- zip (XMonad.workspaces conf) [xK_1,xK_2,xK_3,xK_4,xK_5,xK_6,xK_7,xK_8,xK_9,xK_0]

  --French Azerty users use this line
  -- | (i, k) <- zip (XMonad.workspaces conf) [xK_ampersand, xK_eacute, xK_quotedbl, xK_apostrophe, xK_parenleft, xK_minus, xK_egrave, xK_underscore, xK_ccedilla , xK_agrave]

  --Belgian Azerty users use this line
  -- | (i, k) <- zip (XMonad.workspaces conf) [xK_ampersand, xK_eacute, xK_quotedbl, xK_apostrophe, xK_parenleft, xK_section, xK_egrave, xK_exclam, xK_ccedilla, xK_agrave]

      , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)
      , (\i -> W.greedyView i . W.shift i, shiftMask)]]

-- ++
  -- ctrl-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
  -- ctrl-shift-{w,e,r}, Move client to screen 1, 2, or 3
--  [((m .|. controlMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
  --    | (key, sc) <- zip [xK_w, xK_e] [0..]
    --  , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


main :: IO ()
main =  do

    dbus <- D.connectSession
    -- Request access to the DBus name
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]


    xmonad . ewmh $
  --Keyboard layouts
  --qwerty users use this line
            myBaseConfig
  --French Azerty users use this line
            --myBaseConfig { keys = azertyKeys <+> keys azertyConfig }
  --Belgian Azerty users use this line
            --myBaseConfig { keys = belgianKeys <+> keys belgianConfig }

                {startupHook = myStartupHook
, layoutHook = gaps [(U,35), (D,5), (R,5), (L,5)] $ myLayout ||| layoutHook myBaseConfig
, manageHook = manageSpawn <+> myManageHook <+> manageHook myBaseConfig
, modMask = myModMask
, borderWidth = myBorderWidth
, handleEventHook    = handleEventHook myBaseConfig <+> fullscreenEventHook
, focusFollowsMouse = myFocusFollowsMouse
, workspaces = myWorkspaces
, focusedBorderColor = focdBord
, normalBorderColor = normBord
, keys = myKeys
, mouseBindings = myMouseBindings
}`additionalKeysP` myKeys
